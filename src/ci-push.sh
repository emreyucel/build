#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

REMOTE=$AWS_ACCOUNT_ID.dkr.ecr.eu-central-1.amazonaws.com
NAME=ciceksepeti/%PROJECT_NAME_LOWER%
BRANCHNAME=$(git branch | grep \* | cut -d ' ' -f2 | tr '/' '-' | tr '[:upper:]' '[:lower:]' )
BRANCH=$CIRCLE_BRANCH
BUILDNUMBER=$CIRCLE_BUILD_NUM
eval $(aws ecr get-login --region eu-central-1)

TAG=$BRANCHNAME-$BUILDNUMBER
if [[ $BRANCH =~ ^feature\/.*  ]]; then
	TAG=$BRANCHNAME-$BUILDNUMBER
fi

# Push same image twice, once with the ci build number as the tag, and once with
# 'latest' as the tag. 'latest' will always refer to the last image that was
# built, since the next time this script is run, it'll get overridden. The
# ci build number, however, is a constant reference to this image.

# docker tag "-f" deprecated in docker 10.x So circleci 1.0 using
# docker 1.10 but circleci 2.0 use 17.x we have to check docker version
if [[ $(docker --version | awk '{print $3}')  == 17* ]]; then
	docker tag $NAME $REMOTE/$NAME:$TAG
else
	docker tag -f $NAME $REMOTE/$NAME:$TAG
fi

docker push $REMOTE/$NAME:$TAG

docker logout https://$REMOTE